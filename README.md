# Node API Service Test

This is a simple Node API for email records

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

a Demo Service also deployed to Heroku and can be accessed at : https://heroku-node-api-backend-sample.herokuapp.com/email

### Prerequisites

You need to have node and npm installer to run this application

you also need to have either local or remote PostgreSQL instance and supply the required credentials on the .env file

an init.sql file is provided to easily create the needed schema


### Installing

after you clone the repository, just simply enter these command in the console

```
npm install
```

to start the application you need to run

```
npm start
```

it'll serve the service on localhost:3002/email

### Testing
a postman_collection.json file is provided and can be imported in you Postman Application.
don't forget to set the url value in the Postman Environment.

it can either be https://heroku-node-api-backend-sample.herokuapp.com for testing the demo service on Heroku
or localhost:3002 for testing your local service

### API Documentation
After you install all the node dependencies, you can serve the Included API Documentation with

```
npm run serve-doc
```

it'll serve the decumentation on localhost:8000

You can also generate static web file of the documentation by running `npx redoc-cli bundle -o index.html swagger.yaml`
the resulting HTML file can then be hosted online