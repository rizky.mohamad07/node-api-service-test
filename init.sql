CREATE TABLE emails (
  ID SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) UNIQUE NOT NULL
);

INSERT INTO emails (name, email)
VALUES  ('test', 'test@email.com');