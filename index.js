const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { pool } = require("./config");

const { check, validationResult } = require("express-validator");
const helmet = require('helmet')
const compression = require('compression')

const dbHelper = require("./Helper/databaseHelper");
const responseHelper = require("./Helper/responseHelper");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(compression())
app.use(helmet())

const getEmailRecords = async (request, response) => {
  try {
    const queryResult = await dbHelper.getAsync(
      pool,
      "SELECT * FROM emails",
      []
    );
    response
      .status(200)
      .json(responseHelper.generateResponse(true, {data : queryResult}));
  } catch (e) {
    if (e.severity) {
      //database related error
      response
        .status(500)
        .json(responseHelper.generateResponse(false, [], e.detail));
    } else {
      //non database related error
      response
        .status(500)
        .json(responseHelper.generateResponse(false, [], e.message));
    }
  }
};

const addEmailRecord = async (request, response) => {
  const { name, email } = request.body;
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return response
      .status(400)
      .json(
        responseHelper.generateResponse(false, { email: email }, "Is Invalid")
      );
  }
  try {
    const queryResult = await dbHelper.runAsync(
      pool,
      "INSERT INTO emails (name, email) VALUES ($1, $2) RETURNING id",
      [name, email]
    );
    const emailDetail = await dbHelper.getAsync(
      pool,
      "SELECT * FROM emails WHERE id=$1",
      [queryResult.rows[0].id]
    );
    response
      .status(201)
      .json(responseHelper.generateResponse(true, emailDetail[0]));
  } catch (e) {
    if (e.severity) {
      //database related error
      if (e.code == "23505" && e.constraint == "emails_email_key") {
        response
          .status(409)
          .json(
            responseHelper.generateResponse(
              false,
              { email: email },
              "Email has already been used"
            )
          );
      } else {
        response
          .status(500)
          .json(responseHelper.generateResponse(false, {}, e.detail));
      }
    } else {
      //non database related error
      response
        .status(500)
        .json(responseHelper.generateResponse(false, [], e.message));
    }
  }
};

app
  .route("/email")
  // GET endpoint
  .get(getEmailRecords)
  // POST endpoint
  .post([check("email").isEmail()], addEmailRecord);

// Start server
app.listen(process.env.PORT || 3002, () => {
  console.log(`Server listening`);
});
