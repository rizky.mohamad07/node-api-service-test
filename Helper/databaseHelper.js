"use strict";

module.exports.getAsync = function(db, sql, param) {
  return new Promise((resolve, reject) => {
    db.query(sql, param, function(err, result) {
      if (err) {
        reject(err);
      }
      resolve(result.rows);
    });
  });
};

module.exports.runAsync = function(db, sql, param) {
  return new Promise((resolve, reject) => {
    db.query(sql, param, function(err, result) {
      if (err) {
        reject(err);
      }
      resolve(result);
    });
  });
};
