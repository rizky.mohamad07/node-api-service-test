"use strict";

module.exports.generateResponse = function(status, data, message) {
  let response = {};

  response.status = status;

  if (message) {
    response.message = message;
  }

  if (data) {
    response = { ...data, ...response };
  }

  return response;
};
